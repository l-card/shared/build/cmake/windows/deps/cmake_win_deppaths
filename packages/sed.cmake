include(${WINDEPPATHS_PACKAGES_DIR}/cygwin.cmake)

if(NOT SED_EXEC)
    set(SED_EXEC "${CYGWIN_DIR}/bin/sed")
endif(NOT SED_EXEC)
