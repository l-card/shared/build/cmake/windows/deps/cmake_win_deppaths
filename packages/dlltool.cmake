include(${WINDEPPATHS_PACKAGES_DIR}/cygwin.cmake)

if(NOT DLLTOOL64_EXEC)
    set(DLLTOOL64_EXEC "${CYGWIN_DIR}/bin/x86_64-w64-mingw32-dlltool.exe")
endif(NOT DLLTOOL64_EXEC)

if(NOT DLLTOOL32_EXEC)
    set(DLLTOOL32_EXEC "${CYGWIN_DIR}/bin/i686-w64-mingw32-dlltool.exe")
endif(NOT DLLTOOL32_EXEC)