if(NOT NET_FRAMEVORKv2.0_DIR)
    set(NET_FRAMEVORKv2.0_DIR c:/Windows/Microsoft.NET/Framework/v2.0.50727)
endif(NOT NET_FRAMEVORKv2.0_DIR)

if(NOT NET_FRAMEVORKv3.5_DIR)
    set(NET_FRAMEVORKv3.5_DIR c:/Windows/Microsoft.NET/Framework/v3.5)
endif(NOT NET_FRAMEVORKv3.5_DIR)

if(NOT NET_FRAMEVORKv4.0_DIR)
    set(NET_FRAMEVORKv4.0_DIR c:/Windows/Microsoft.NET/Framework/v4.0.30319)
endif(NOT NET_FRAMEVORKv4.0_DIR)

