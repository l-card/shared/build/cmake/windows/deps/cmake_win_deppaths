if(NOT VS_ROOT_DIR)
    set(VS_ROOT_DIR "${PROGRAM_DIR32}/Microsoft Visual Studio 14.0")
endif(NOT VS_ROOT_DIR)

if(NOT VS_SETENV_SCRIPT)
    set(VS_SETENV_SCRIPT "${VS_ROOT_DIR}/VC/vcvarsall.bat")
endif(NOT VS_SETENV_SCRIPT)

