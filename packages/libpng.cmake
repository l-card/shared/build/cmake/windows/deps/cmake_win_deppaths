if(NOT PNG_ROOT_DIR)
    set(PNG_ROOT_DIR ${EXTERN_LIB_DIR}/libpng)
endif(NOT PNG_ROOT_DIR)

if(NOT PNG_INCLUDE_DIR)
    set(PNG_INCLUDE_DIR "${PNG_ROOT_DIR}/include")
endif(NOT PNG_INCLUDE_DIR)

if(NOT PNG_INCLUDE_DIRS)
    set(PNG_INCLUDE_DIRS "${PNG_INCLUDE_DIR}")
endif(NOT PNG_INCLUDE_DIRS)

if(NOT PNG_LIBRARY)
    set(PNG_LIBRARY "${PNG_ROOT_DIR}/lib/libpng16.lib")
endif(NOT PNG_LIBRARY)

if(NOT PNG_LIBRARIES)
    set(PNG_LIBRARIES "${PNG_LIBRARY}")
endif(NOT PNG_LIBRARIES)

if(NOT PNG_DLL)
    set(PNG_DLL "${PNG_ROOT_DIR}/bin/libpng16.dll")
endif(NOT PNG_DLL)
