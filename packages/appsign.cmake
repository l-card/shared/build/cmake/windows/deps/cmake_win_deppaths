include(${WINDEPPATHS_PACKAGES_DIR}/winkit.cmake)

if(NOT SIGNTOOL_EXEC)
    set(SIGNTOOL_EXEC "${WINKIT_DIR}/App Certification Kit/signtool.exe")
endif(NOT SIGNTOOL_EXEC)

if(NOT SIGN_CERT_DIR)
    set(SIGN_CERT_DIR ${EXTERN_LIB_DIR}/cert)
endif(NOT SIGN_CERT_DIR)

if(NOT SIGN_CROSSCERT)
    set(SIGN_CROSSCERT ${SIGN_CERT_DIR}/DigiCertHighAssuranceEVRootCA.crt)
endif(NOT SIGN_CROSSCERT)

if(NOT SIGN_DOTNET_CERT)
    set(SIGN_DOTNET_CERT ${SIGN_CERT_DIR}/lcardNet.snk)
endif(NOT SIGN_DOTNET_CERT)

if(NOT SIGN_COMPANY_NAME)
    set(SIGN_COMPANY_NAME "L Card, LLC")
endif(NOT SIGN_COMPANY_NAME)

if(NOT CERT_TIMESTAMP_URL)
    set(CERT_TIMESTAMP_URL http://timestamp.digicert.com)
endif(NOT CERT_TIMESTAMP_URL)
