include(${WINDEPPATHS_PACKAGES_DIR}/cygwin.cmake)

if(NOT GENDEF_EXEC)
    set(GENDEF_EXEC "${CYGWIN_DIR}/bin/gendef.exe")
endif(NOT GENDEF_EXEC)
