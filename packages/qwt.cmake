if(NOT QWT_ROOT_DIR)
    set(QWT_ROOT_DIR "${PROGRAM_DIR32}/qwt")
endif(NOT QWT_ROOT_DIR)

if(NOT QWT_DLL)
    set(QWT_DLL "${QWT_ROOT_DIR}/lib/qwt.dll")
endif(NOT QWT_DLL)