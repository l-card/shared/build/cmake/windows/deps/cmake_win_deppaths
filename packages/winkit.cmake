if(NOT WINKIT_MAJOR_VERSION)
    set(WINKIT_MAJOR_VERSION 10)
endif(NOT WINKIT_MAJOR_VERSION)

if(NOT WINKIT_DIR)
    set(WINKIT_DIR "${PROGRAM_DIR32}/Windows Kits/${WINKIT_MAJOR_VERSION}")
endif(NOT WINKIT_DIR)
