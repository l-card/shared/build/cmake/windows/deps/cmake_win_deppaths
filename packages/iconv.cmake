include(${WINDEPPATHS_PACKAGES_DIR}/cygwin.cmake)

if(NOT ICONV_EXEC)
    set(ICONV_EXEC "${CYGWIN_DIR}/bin/iconv")
endif(NOT ICONV_EXEC)
