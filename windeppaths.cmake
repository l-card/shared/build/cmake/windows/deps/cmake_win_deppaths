#возможность пользователю задать свой файл с путями ко всем или части зависимостей
if(CMAKE_WINDEPPATHS_USER_FILE)
    include("${CMAKE_WINDEPPATHS_USER_FILE}")
endif(CMAKE_WINDEPPATHS_USER_FILE)


# стандартные пути windows
if(NOT PROGRAM_DIR)
    set(PROGRAM_DIR "c:/Program Files")
endif(NOT PROGRAM_DIR)

if(NOT PROGRAM_DIR32)
    set(PROGRAM_DIR32 "c:/Program Files (x86)")
endif(NOT PROGRAM_DIR32)

if(NOT EXTERN_LIB_DIR)
    set(EXTERN_LIB_DIR ${PROGRAM_DIR32}/libs)
endif(NOT EXTERN_LIB_DIR)

set(WINDEPPATHS_PACKAGES_DIR ${CMAKE_CURRENT_LIST_DIR}/packages)

foreach(WINDEPPATH_PACKAGE ${WINDEPPATH_PACKAGES})
  include(${WINDEPPATHS_PACKAGES_DIR}/${WINDEPPATH_PACKAGE}.cmake)
endforeach()

